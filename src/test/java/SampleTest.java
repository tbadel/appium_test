import core.BaseSettings;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import org.junit.*;
import org.junit.Test;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.thoughtworks.selenium.webdriven.commands.WaitForCondition;

import java.awt.Dimension;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.JVM)
public class SampleTest {
  static AndroidDriver driver;
  WebDriverWait wait = new WebDriverWait(driver, 20);
		@BeforeClass
    public static void setUp() throws Exception {
        BaseSettings settings = new BaseSettings();
        driver = settings.getDriver();
    }
	
		public void bottomTopswipe(int timeduration) {
			  org.openqa.selenium.Dimension  size = driver.manage().window().getSize();
			  System.out.println(size);
			  int starty = (int) (size.height * 0.50);
			  int endy = (int) (size.height * 0.20);
			  int startx = size.width / 2;
			  System.out.println("Start swipe operation");
			  driver.swipe(startx, starty, startx, endy, timeduration);

			 }

		
		
    @Test
    
    public void hamburgermenu_should_be_visible_and_clickable() throws Exception {

    	WebElement menu = driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Main navigation, open\"]"));
        assertTrue(menu.isDisplayed()&&menu.isEnabled());
        
    }

    
    
    
    
    @Test
    public void user_can_add_item_to_cart() throws Exception {
       
    	
    	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.ebay.mobile:id/item_title")));
    	WebElement firstitem = driver.findElement(By.id("com.ebay.mobile:id/item_title"));
               
        firstitem.click();
        Thread.sleep(5000);
        bottomTopswipe(3000);
        
        driver.findElement(By.id("com.ebay.mobile:id/button_add_to_cart")).click();
        
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.ebay.mobile:id/spinner_selection_option")));
        
        driver.findElement(By.id("com.ebay.mobile:id/spinner_selection_option")).click();
        
        driver.findElement(By.xpath("/hierarchy//android.widget.FrameLayout[5]/android.widget.LinearLayout")).click();
        
        Thread.sleep(2000);
        
        assertTrue(driver.findElement(By.id("com.ebay.mobile:id/button_add_to_cart_buybar")).isDisplayed());
    }
    
    
    
    
    
    


    @Test
    public void user_is_able_to_search() throws Exception {
        WebElement toggleBtn = driver.findElement(By.id("com.ebay.mobile:id/search_box"));
        toggleBtn.click();
        driver.findElement(By.id("com.ebay.mobile:id/search_src_text")).sendKeys("bike");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy//android.widget.FrameLayout/android.widget.LinearLayout[2]//android.widget.RelativeLayout[1]/android.widget.TextView")));
        driver.findElement(By.xpath("/hierarchy//android.widget.FrameLayout/android.widget.LinearLayout[2]//android.widget.RelativeLayout[1]/android.widget.TextView")).click();
        
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(" /hierarchy//android.widget.RelativeLayout[1]//android.widget.TextView[2] ")));
      String searchtext=  driver.findElement(By.xpath(" /hierarchy//android.widget.RelativeLayout[1]//android.widget.TextView[2]")).getText();
        
        assertTrue(searchtext.toLowerCase().contains("bike"));
                 }


    @AfterClass
    public static void tearDown() throws Exception {
    
        driver.quit();
    }
    
   
}
