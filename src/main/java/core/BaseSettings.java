package core;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseSettings {

    public AndroidDriver driver;

    public AndroidDriver getDriver() throws MalformedURLException {


        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus_5_API_24");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.0");        
        capabilities.setCapability(MobileCapabilityType.APP, "C:\\Users\\ysaleem\\Desktop\\com.ebay.mobile_2018-11-21.apk");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
        capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.10.0");
        if (driver == null)
            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);


        return driver;
    }
}
